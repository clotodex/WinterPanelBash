# Winterpanel

- modular (either source scripts, or precompile to make one huge but faster one)
- highly configurable
- multi monitor support
- own mini scripts -> different types of scripts
- positioning config
- dzen clone for mouse exit and hover
- theme support (swatch?)
- multiple panel support?

- Herbstluft provides Eventsystem --> even more herbst products can use this now
- develop protocol for message transfer

- 3 Main parts of software:
	- WinterPanel -> decodes hooks and sends changes to dzen, sends refresh all hook
	- WinterSheduler -> shedules timesensitive components (calls them)
	- Component -> emits hook properly formatted

- Problem: how to start stuff (especially the Sheduler)	
	- config to tell what goes where -> start the what and place it there
	- file with list of components that need sheduling, and their sheduling times
	- shedule hook?
	
- Hook format:

Event:
wp-event	compoent-name	dzen-code	optional:pixel-width	optional:confirm-receive

Confirm:
wp-confirm	component-name

Refresh:
wp-refresh	60 30 15 10 5 1 (list all refresh intervals)
wp-reload	component-name|\*	optional:more-components

Request:
wp-request-refresh	component-name	interval-in-seconds
