#!/bin/bash

#TODO save all background processes started and stop them as soon as this process is killed

#--------------------STATIC--------------------#

###############
# static vars #
###############

CONFIG_FILE="winterpanel.config"
ALIASES_FILE="alias.config"
COMPONENTS_FOLDER="components"

#--------------------FUNCTIONS--------------------#

###########
# logging #
###########

function log {
	#TODO pipe around dzen with >9 and 9>1 or sth like this 
	echo "[LOG] $*"
	#herbstclient emit_hook "[LOG]" "$*"
	:;
}

function debug {
    echo "[DEBUG] $*"
	#herbstclient emit_hook "[DEBUG]" "$*"
	:;
}

#########
# dzen2 #
#########

buffer_for_dzen() {
	echo -n "$@" >&4
}

commit_to_dzen() {
    echo "$@" >&4
}

calculate_width(){
	#TODO take padding into the calculation
	local font="$1"
    local text="$2"
	log "calculating width of $text"
	local text="$(clean_from_dzen_instructions "$text")"
	log "calculating width of $text"
    width=$($textwidth "$font" "$text")  
	echo "$width"
}

#currently unused
createPopopBash(){
	#TODO dont go over panelwidth (x + DEFAULT_WIDTH !> panel_width
	#TODO detect display first
	x="$1"
	y="$2"
	title="$3"
	text="$4"
	#TODO instead of sleep 5 detect if mouse posiition is in rectangle - if not close
	echo "{ echo \""$title"\"; echo -e \""$text"\"; sleep 5; } | dzen2 -fn "$font" -x $x -y $y -w $DEFAULT_WIDTH -l 5"
}

clean_from_dzen_instructions() {
	#FIXME remove unnecessary assignment
	text=$(echo -n "$1" | sed 's.\^[^(]*([^)]*)..g')
	#text=$(echo -n "$1" | sed 's.\^[^\(]*\([^\)]*\)..g')
	#text=$(echo -n "$1" | sed -n -r -e 's/\^[^\(]*\([^\)]*\)//g')

	echo "$text"
}

function replaceComponents {
	local text="$1"
	local total_amount=0
	#search for each registered component and replace it :)
	for eventname in "${!events[@]}"
	do
        local message="${events[$eventname]}"
		text="${text//"{${eventname}}"/"${message}"}"
		local amount=${msg_len[$event]}
		total_amount="$((total_amount+amount))"
	done
	echo "$text"
}


function update_dzen {
	log "Updating dzen!"

	#FIXME should be in the setup section (just do this once)
    herbstclient pad $panel_height

	#TODO send hook [wp] action componentname hover/click

	#buffer: echo -n
	#commit: echo

	#FIXME how to handle overlapping lines?

	#TODO replace components - tmp variables?
	#TODO add length together (components + left without variables)- tmp variables?

	### left ###
	#$left

	### center ###
	#pos: width-(width/2)-(center_width/2)-left_width

	### right ###
	#pos: width-left_width-padding-center-right_width

	commit_to_dzen "$(replaceComponents $left)$(replaceComponents $center)$(replaceComponents $right)"
}

#--------------------MAIN--------------------#

#stop all other panels
herbstclient emit_hook quit_panel

###############
# load config #
###############

#input:  configfile
#output: list of components
#        placement of components
#        configuration of panel behavior and looks

function create_dzen_command {
	echo "dzen2 -w ${panel_width} -x ${x} -y ${y} -fn "${panel_font}" -h ${panel_height} \
    -e 'button3=;button4=exec:herbstclient use_index -1;button5=exec:herbstclient use_index +1' \
    -ta l -bg '"${bgcolor}"' -fg '#efefef'"
}

##### Default Config #####
panel_height=16
panel_font="-*-fixed-medium-*-*-*-12-*-*-*-*-*-*-*"
bgcolor=$(herbstclient get frame_border_normal_color)
panel_bg=$(herbstclient get window_border_active_color)
panel_fg="#101010"

#import config
source "${CONFIG_FILE}"

### create dzen commands ###
dzen_command="tee"
declare -A monitors
#loop over all available screens and contruct dzen command which then gets piped into via eval
for monitor in $(herbstclient list_monitors | cut -d: -f1) ; do
	geometry=( $(herbstclient monitor_rect "$monitor") )
	if [ -z "$geometry" ] ;then
		echo "Invalid monitor $monitor"
		exit 1
	fi
	# geometry has the format W H X Y
	x=${geometry[0]}
	y=${geometry[1]}
	panel_width=${geometry[2]}
    monitors[$monitor]="$x:$y"

	dzen_command="${dzen_command} >($(create_dzen_command))"
done
debug "dzen_command: ${dzen_command}"

exec 4> >(eval "$dzen_command")


# Try to find textwidth binary.
# In e.g. Ubuntu, this is named dzen2-textwidth.
if which textwidth &> /dev/null ; then
    textwidth="textwidth";
elif which dzen2-textwidth &> /dev/null ; then
    textwidth="dzen2-textwidth";
else
    echo "This script requires the textwidth tool of the dzen2 project."
    exit 1
fi

#concat all events
all="${left}${center}${right}"
#filter out events
declare -A events
while read -r event
do
    echo "event found: $event"
	events[$event]="loading..."
done <<< "$(echo "$all" | grep -o -P "(?<={)[^}]*(?=})")"

debug "found events: ${!events[@]}"

#read in aliases
#--> one file can put out multiple events
declare -A aliases
while IFS=\= read -r event component
do
    aliases[$event]=$component
done < "$COMPONENTS_FOLDER/$ALIASES_FILE"

#some debug messages
debug "Aliases found:"
for event in ${!aliases[@]}
do
    debug "$event - ${aliases[$event]}"
done

#now figure out exactly which components need to be started

declare -A components
#iterate over events
for event in ${!events[@]}
do
	#fill new array with component names
	component="${aliases[$event]}"
	#filter out double entries of array (little hack with associative arrays)
	components[${component}]=1
done
debug "comps: ${!components[@]}"

#TODO
#each component will lock itsself from not starting again
#or: send a reload * event and exclude those who answer within a second?? TIMING

###########
# startup #
###########

#components.foreach(start_binary)
for comp in ${!components[@]}
do
	./$COMPONENTS_FOLDER/$comp &
	components[$comp]="$!"
done
#TODO configure components?
#TODO listen for first line of output? instant configuration for sheduler


#####################
# listen for events #
#####################

#store old event message in associative array to make updating easier

#create mirror of events array to store message length
declare -A msg_len
for event in ${!events[@]}
do
    msg_len[$event]=0
done

log "Listening for events"

#setup listener
herbstclient --idle | {
while true
do
IFS=$'\t' read -ra parts || break
	#log "EVENT"
	#detect if event a winterpanel event
	#if [[ "$event_line" == "[wp]"* ]];then
	case "${parts[0]}" in
	"[wp]")
		debug "Detected Event of type: ${parts[1]}"

		#separate line into parts (and keep spaces)
		#OIFS=$IFS
		#IFS=$'\t'
		#parts=($event_line)
		#IFS=$OIFS

		#type of event:
        case "${parts[1]}" in
			#event: prepare for dzen
			event)
				#parts[2] is event name
				event="${parts[2]}"
				#parts[3] is dzen message
				message="${parts[3]}"
				#parts[4] is textwidth
				msg_len[$event]="${parts[4]}"
				#TODO calculate length here when not supplied
				#parts[5] is optional: confirm-receive
				if [ "${parts[5]}x" = "confirmx" ]
				then
					herbstclient emit_hook "[wp]" "confirm" "$event" 
				fi

				#check if msg is the same as before only on different -- update dzen
				if [ "${events[$event]}x" != "${message}x" ]
				then
					events[$event]="${message}"
					update_dzen
				fi
				;;
			reload)
				if [ "${parts[2]}x" = "panelx" ]
				then
					#TODO stop components, clear arrays, reload config/aliases, clear dzen, emit reload hook
					:;
				fi
				;;
			#request: handle request
			request)
				#TODO handle request (types: sheduling, ...?)
				#TODO fill timing array & stop and start sheduler
				;;
			#answer to ping is pong?
			ping)
				#parts[2] is an identifier (normally the event name) to send the pong to
				herbstclient emit_hook "[wp]" "pong" "${parts[2]}"
				;;
			pong|confirm|action|quit)
				;; #ignore
			*)
				log "Unknown WP-Event: ${parts[1]}"
				;;
		esac
	#TODO also quit on reload hook, or add quit_panel to autostart
	#elif [ "${event_line}x" = "quit_panelx" ]
	#then
	;;
	reload|quit_panel)
		log "quitting..."
		#FIXME this is the only way to force the panel to close (does read block?)
		sleep 1 && herbstclient emit_hook "[wp]" "quit" "*" &
        # TODO stop components, clear/close dzen
		#this somehow doesnt kill instantly (needs one more event to close)
		exit
	#fi
	;;
	esac
done
}
#} 2> /dev/null | eval "$dzen_command"

#TODO setup sheduler to call reload to specific components
#iterate with smallest seconds requested

